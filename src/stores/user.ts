import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/users";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const users = ref<User[]>([]);
  const editedUser = ref<User>({ login: "", name: "", password: "" });
  const delId = ref();
  const delDialog = ref(false);

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedUser.value = { login: "", name: "", password: "" };
    }
  });
  async function getUser() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึง User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }

      dialog.value = false;

      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกได้");
    }
    loadingStore.isLoading = false;
  }

  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await userService.deleteUser(id);
      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบได้");
    }
    loadingStore.isLoading = false;
    delDialog.value = false;
  }

  return {
    users,
    getUser,
    dialog,
    editedUser,
    saveUser,
    editUser,
    deleteUser,
    delId,
    delDialog,
  };
});
